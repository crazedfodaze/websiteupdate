

# Graylog AWS Install Guide

Use this tutorial for step-by-step instructions for installing Graylog on an AWS EC2 instance. 
*Links and packages reflect those available on the article publish date.* 

## Amazon Machine Image (AMI) 

- **Image type:**  Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type - ami-033b95fb8079dc481 (64-bit x86) / ami-0f7691f59fd7c47af (64-bit Arm)  

- For demo purposes, we will be using **t2. medium** 

> A t2.small can also be used, but you might experience memory exhaustion issues. 

## Security Group Configuration

The default install of Graylog will require port 9000 open for access. 

> For best security practices, configure security rules to allow connections only from IPs used for Graylog management. 

## Prerequisites  

- Update new instance 

    `$ sudo yum update -y`

### Install Java version 17.02

- Download Java 17 rpm package from official Oracle website:

    ` wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.rpm` 

- As this package is not signed, we will confirm the integrity of the downloaded file by comparing its sha256 hash provided by Oracle. 

    `sha256sum jdk-17_linux-x64_bin.rpm` 

- Compare output with [sha256 provided by Oracle:](https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.rpm.sha256)
    >53589079528059b9617ece9ecfd68e96c3bb0a7796f8a9a580d4057288f23239

- Install Java 17

    `$ sudo rpm -i jdk-17_linux-x64_bin.rpm`
    
- Check Java version to confirm install

    ` java --version`

- As it is no longer needed delete jdk-17_linux-x64_bin.rpm 

    `rm jdk-17_linux-x64_bin.rpm`

### Install MongoDB

[Official AWS EC2 MongoDB installation instructions.](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-amazon/)

- Configure MongoDB repository configuration<br>
<i>Use command line editor of your choice</i>

    `$ sudo vim /etc/yum.repos.d/mongodb-org-5.0.repo` 
    
    - Copy repository information below and paste in to open elasticsearch.repo file

    ```
    [mongodb-org-5.0]
    name=MongoDB Repository
    baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/5.0/x86_64/
    gpgcheck=1
    enabled=1
    gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc 
    ```

- Install the latest stable version of MongoDB.

    `$ sudo yum install -y mongodb-org`

- Start MongoDB 

    `$ sudo systemctl start mongod.service`
    
- Confirm MongoDB is running
   
   `systemctl status mongod.service`

* Configure MongoDB to start following a system reboot 

    `$ sudo systemctl enable mongod.service`

### Install Elasticsearch

[Official Elasticsearch AWS EC2 installation instructions](https://www.elastic.co/blog/running-elasticsearch-on-aws)

- Install the Elasticsearch GPG Key 

    `$ sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch`

- Configure Elasticsearch repository configuration<br>
<i>Use command line editor of your choice</i>
    
    `$ sudo vim /etc/yum.repos.d/elasticsearch.repo`

- Copy repository information below and paste in to open elasticsearch.repo file
    
    ```
    [elasticsearch-7.x]
    name=Elasticsearch repository for 7.x packages
    baseurl=https://artifacts.elastic.co/packages/oss-7.x/yum
    gpgcheck=1
    gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
    enabled=1
    autorefresh=1
    type=rpm-md
    ``` 
 
- Install the latest release of Elasticsearch 

    `$ sudo yum install elasticsearch-oss -y`

- Modify the Elasticsearch configuration file (/etc/elasticsearch/elasticsearch.yml) to set the cluster name to graylog and uncomment action.auto_create_index: false to enable the action.

    `$ sudo tee -a /etc/elasticsearch/elasticsearch.yml > /dev/null <<EOT
cluster.name: graylog
action.auto_create_index: false
EOT
`
>  Whenever you make any changes to the systemd unit file, you will need to reload the systemd to pick up those changes.

-  Reload configuration files

    `$ sudo systemctl daemon-reload`

- Restart Elasticsearch

    `$ sudo systemctl restart elasticsearch.service`

- Confirm Elasticsearch is running

    `systemctl status elasticsearch.service`

- Configure Elasticsearch to start following a system reboot

    `$ sudo systemctl enable elasticsearch.service`

## Installing Graylog

- Configure Graylog repository configuration

    `$ sudo rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm`

- Install Graylog packages

    `$ sudo yum install graylog-server graylog-enterprise-plugins graylog-integrations-plugins graylog-enterprise-integrations-plugins`

- Start Graylog

    `$ sudo systemctl start graylog-server.service`

- Confirm Graylog is running

    `systemctl status graylog-server.service`

- Configure Graylog to start following a system reboot

    `$ sudo systemctl enable graylog-server.service`





