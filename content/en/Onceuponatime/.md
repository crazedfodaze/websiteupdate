
**About [Choose Linux](https://chooselinux.show):**

Every 2 weeks, Joe, Ell, and Drew talk about what they've discovered in the world of Linux and Open Source.

* [12: Regolith, Rosa, and Antsy Alien Attack](https://chooselinux.show/12) Two new hosts join Joe to talk about a nice i3 implementation, and an amazing arcade game written in Bash.

* [13: Qubes OS + Plex vs Kodi](https://chooselinux.show/13) Distrohoppers brings us a fascinating distro where every application runs in its own VM. Plus Drew and Joe disagree on the best media solution.

* [14: Endeavour OS + Pisi Linux](https://chooselinux.show/14) We take a look at the continuation of Antergos called Endeavour OS and are pretty impressed, and Distrohoppers delivers an interesting distro that's obsessed with cats.

* [15: OBS Studio + Endless OS](https://chooselinux.show/15) Distrohoppers delivers a distro that divides us, and we check out the streaming and recording software OBS Studio.

* [16: PCLinuxOS + Hugo](https://chooselinux.show/16) We check out a great tool for learning web development basics, and Distrohoppers brings us mixed experiences.

* [17: Hardware hacking basics, Slackel + OSCAR](https://chooselinux.show/17) Getting into hardware hacking with Arduino, and analysing sleep data from CPAP machines.

* [18: Introducing New People to Linux](https://chooselinux.show/18) There's lots to consider when setting someone up with Linux for the first time. User needs and expectations, distro choice, hardware, and so much more. We discuss our experiences, and ask some fundamental questions. 

* [19: Android-x86 + First Steps into the Cloud](https://chooselinux.show/19) We have three different approaches to using the cloud, so we discuss various ways to expand your Linux knowledge beyond the desktop. Plus Distrohoppers delivers a mobile-like experience that splits opinion. 

* [20: Single Board Computers](https://chooselinux.show/20) We are joined by special guest Chz who is a long-time user of single board computers to talk about how we use boards like the Raspberry Pi, Orange Pi, and ROCKPro64.

* [21: KaOS + How We Install Software](https://chooselinux.show/21) There are numerous ways to install software on a modern Linux system and we each have a different approach.

* [22: Finding Your Community](https://chooselinux.show/22) We talk about the best ways to get involved in open source communities, finding like-minded people, conference strategies, community hubs, and what happened to all the LUGs.

* [23: Void Linux + Contributing to Open Source](https://chooselinux.show/23)A chance to learn some Linux fundamentals in Distrohoppers, and the numerous ways we can all contribute to Linux and open source. 

* [24: What We Wish We'd Known Earlier](https://chooselinux.show/24) All three of us have different levels of experience with Linux but there are tons of things that we wish we'd learned earlier in our journey.

* [25: Tails + Virtualization](https://chooselinux.show/25) Ultimate privacy in Distrohoppers, and the best ways to run other operating systems within your current Linux distro.

* [26: Explaining Linux and Open Source as Concepts](https://chooselinux.show/26) Trying to explain what Linux and open source are can be tricky. We discuss our various approaches, and how they differ depending on the experience of who we are explaining them to.

* [27: GhostBSD + Freedom vs Pragmatism](https://chooselinux.show/27) Distrohoppers serves up something very different in the form of desktop BSD, and we reveal how important freedom is to us all.

* [28: What We Love About Linux](https://chooselinux.show/28) Valentine's Day is nearly here so it's time to talk about why we love Linux and open source. Nothing is perfect though, so we also touch on a few areas that we feel could be improved.

* [29: Linux Console + Boutique Distros](https://chooselinux.show/30) A confusing experience in Distrohoppers which raises deeper questions about the value and viability of smaller distros.

* [30: Project Catch Up](https://chooselinux.show/30) We revisit some of the projects we have covered in previous episodes to see what we've stuck with and what we haven't.

* [31: Solus + Visual Studio Code](https://chooselinux.show/31) We try out Solus and are all impressed by this independent distro. Then Ell and Drew sing the praises of Visual Studio Code - a text editor that's packed full of features.

* [32: Windows as a Linux User + Sway Window Manager](https://chooselinux.show/32) Ell tells us about her first ever experience with Windows 10 and how it compares with Linux. Plus Drew has been using a Wayland-based i3-like tiling window manager called Sway.
